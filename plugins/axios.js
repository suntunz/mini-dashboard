import { setClient } from '~/services/apis'

export default ({ $axios }) => {
  $axios.setHeader('Content-Type', 'application/json')
  setClient($axios)
}
