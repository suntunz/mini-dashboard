FROM node:12-alpine as build

WORKDIR /app

ADD package.json yarn.lock ./
RUN yarn install --production

COPY . .
RUN yarn build

FROM node:12-alpine
WORKDIR /dir
COPY --from=build /app /dir
EXPOSE 3000
CMD ["yarn", "start"]
