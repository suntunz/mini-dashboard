import {
  fetchUsersRequest, createUserRequest, updateUserRequest, deleteUserByIdRequest
} from '~/services/apis'
import { actionMiddleware } from '~/services/store/actionMiddleware'

export const eventsFetchUsers = {
  PENDING: 'FETCH_USERS',
  FULFILLED: 'FETCH_USERS_SUCCEEDED',
  REJECTED: 'FETCH_USERS_FAILED'
}
export const eventsCreateUser = {
  PENDING: 'CREATE_USER',
  FULFILLED: 'CREATE_USER_SUCCEEDED',
  REJECTED: 'CREATE_USER_FAILED'
}
export const eventsUpdateUser = {
  PENDING: 'UPDATE_USER',
  FULFILLED: 'UPDATE_USER_SUCCEEDED',
  REJECTED: 'UPDATE_USER_FAILED'
}
export const eventsDeleteUserById = {
  PENDING: 'DELETE_USER_BY_ID',
  FULFILLED: 'DELETE_USER_BY_ID_SUCCEEDED',
  REJECTED: 'DELETE_USER_BY_ID_FAILED'
}

export default {
  async fetchUsers(store, payload) {
    const action = {
      types: Object.values(eventsFetchUsers),
      promise: fetchUsersRequest(payload)
    }
    return actionMiddleware(action, store)
  },
  async createUser(store, payload) {
    const action = {
      types: Object.values(eventsCreateUser),
      promise: createUserRequest(payload)
    }
    return actionMiddleware(action, store)
  },
  async updateUser(store, payload) {
    const action = {
      types: Object.values(eventsUpdateUser),
      promise: updateUserRequest(payload)
    }
    return actionMiddleware(action, store)
  },
  async deleteUser(store, payload) {
    const action = {
      types: Object.values(eventsDeleteUserById),
      promise: deleteUserByIdRequest(payload)
    }
    return actionMiddleware(action, store)
  }
}
