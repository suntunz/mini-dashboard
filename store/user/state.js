import { PENDING } from '~/services/store/status'

export default () => ({
  users: {
    response: [],
    status: PENDING,
    error: null
  },
  createUser: {
    response: {},
    status: PENDING,
    error: null
  },
  updateUser: {
    response: {},
    status: PENDING,
    error: null
  },
  deleteUser: {
    response: {},
    status: PENDING,
    error: null
  }
})
