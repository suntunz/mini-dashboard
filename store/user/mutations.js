import { FULFILLED, PENDING, REJECTED } from '~/services/store/status'
import {
  eventsFetchUsers, eventsCreateUser, eventsUpdateUser, eventsDeleteUserById
} from '~/store/user/actions'

export default {
  [eventsFetchUsers.PENDING](state) {
    state.users.status = PENDING
    state.users.error = null
  },
  [eventsFetchUsers.FULFILLED](state, payload) {
    state.users.response = payload
    state.users.status = FULFILLED
    state.users.error = null
  },
  [eventsFetchUsers.REJECTED](state, payload) {
    state.users.response = []
    state.users.status = REJECTED
    state.users.error = payload
  },

  [eventsCreateUser.PENDING](state) {
    state.createUser.status = PENDING
    state.createUser.error = null
  },
  [eventsCreateUser.FULFILLED](state, payload) {
    state.createUser.response = payload
    state.createUser.status = FULFILLED
    state.createUser.error = null
  },
  [eventsCreateUser.REJECTED](state, payload) {
    state.createUser.response = {}
    state.createUser.status = REJECTED
    state.createUser.error = payload
  },

  [eventsUpdateUser.PENDING](state) {
    state.updateUser.status = PENDING
    state.updateUser.error = null
  },
  [eventsUpdateUser.FULFILLED](state, payload) {
    state.updateUser.response = payload
    state.updateUser.status = FULFILLED
    state.updateUser.error = null
  },
  [eventsUpdateUser.REJECTED](state, payload) {
    state.updateUser.response = {}
    state.updateUser.status = REJECTED
    state.updateUser.error = payload
  },

  [eventsDeleteUserById.PENDING](state) {
    state.deleteUser.status = PENDING
    state.deleteUser.error = null
  },
  [eventsDeleteUserById.FULFILLED](state, payload) {
    state.deleteUser.response = payload
    state.deleteUser.status = FULFILLED
    state.deleteUser.error = null
  },
  [eventsDeleteUserById.REJECTED](state, payload) {
    state.deleteUser.response = {}
    state.deleteUser.status = REJECTED
    state.deleteUser.error = payload
  },

  // custom update user
  customUpdateUserById(state, payload) {
    state.users.response = payload
  },

  // custom delete user id
  customDeleteUserByid(state, payload) {
    state.users.response = payload
  }
}
