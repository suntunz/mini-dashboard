export default {
  getUsers(state) {
    return (state.users && state.users.response) || []
  }
}
