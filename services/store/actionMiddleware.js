export const actionMiddleware = async (action, store) => {
  const { types, promise } = await action
  if (types) {
    const [pending, success, error] = types
    store.commit(pending)
    return new Promise((resolve) => {
      promise.then((res) => {
        store.commit(success, res)
        resolve(res)
      }).catch((err) => {
        if (err && err.response && err.response.data) {
          store.commit(error, err.response.data)
          resolve(err.response)
        } else {
          store.commit(error, err)
          resolve(err)
        }
      })
    })
  }
  return store.commit(action.type)
}
