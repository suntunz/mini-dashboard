let $axios

export const setClient = (newclient) => { $axios = newclient }

// Fetch users
export const fetchUsersRequest = () => $axios.$get('users')

// Create new user
export const createUserRequest = (params) => $axios.$post('users', { ...params })

// Update user
export const updateUserRequest = (params) => $axios.$put(`users/${params.id}`, { ...params })

// Delete user by id
export const deleteUserByIdRequest = (userId) => $axios.$delete(`users/${userId}`)
